<%@page import="java.io.PrintWriter"%>
<%@page import="com.example.servletjspdemo.domain.Car"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>deleteCar</title>
</head>
<body>
<h1>DELETED CARS FROM BASKET<br>----------------------------------------------------------</h1>
<jsp:useBean id="storage" class="com.example.servletjspdemo.service.CarStorageService" scope="application" />
<jsp:useBean id="storageB" class="com.example.servletjspdemo.service.BasketStorageService" scope="session" />
<br>
<ul>
<% 
	String[] ids = request.getParameterValues("toDelete");
	String brand = "";
	String model = "";
	int amount = 0;
	int price = 0;
	int allCash=0;
	int thiscarscash = 0;
	for (String id : ids){
			for (Map.Entry<Car, Integer> entry : storageB.getAllCars().entrySet()){
				if (entry.getKey().getId().contains(id)){
					amount = entry.getValue();
					brand = entry.getKey().getBrand();
					model = entry.getKey().getModel();
					price = entry.getKey().getPrice();
					entry.setValue(entry.getValue()-(amount)); 
					out.print("<li>"+amount+" x "+brand+" "+model+"  - "+price+"$  ("+(amount * price)+"$)"+"</li>");
					allCash+=(amount * entry.getKey().getPrice());
					//zeby dodawalo do istniejacego zamowienia
					/*if(storageB.getAllCars().size()>0)
						for (Map.Entry<Car, Integer> entry1 : storageB.getAllCars().entrySet()){
							if(entry1.getKey().getBrand().contains(brand) && entry1.getKey().getModel().contains(model))
								entry1.setValue(entry1.getValue()+amount);
							else
								storageB.add(new Car(brand,model,price),amount);
						}
					else*/
						//storageB.delete(id);
				}
				
			}
		}
	
	
%>
</ul>
<u><b>Everything:</b> <% out.print(allCash); %>$</u>
<p>
<a href="showBasket.jsp">SHOW BASKET</a>
</p>
<p>
<a href="showAllCars.jsp">BUY ANOTHER CAR</a>
</p>

</body>
</html>
