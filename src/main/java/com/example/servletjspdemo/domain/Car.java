package com.example.servletjspdemo.domain;

import java.util.UUID;

public class Car {
	
	private String id;
	private String brand = "Unknown";
	private String model = "Unknown";
	private int	price = 0;
	
	public Car(){
		super();
	}
	
	public Car(String brand,String model,int price){
		super();
		this.id = UUID.randomUUID().toString();
		this.brand = brand;
		this.model = model;
		this.price = price;
	}
	public Car(String brand,String model,int price,String id){
		super();
		this.id = id;
		this.brand = brand;
		this.model = model;
		this.price = price;
	}
	public String getId(){
		return this.id;
	}
	public String getBrand(){
		return this.brand;
	}
	public void setId(String id){
		this.id=id;
	}
	public void setBrand(String brand){
		this.brand = brand;
	}
	public String getModel(){
		return this.model;
	}
	public void setModel(String model){
		this.model = model;
	}
	public int getPrice(){
		return this.price;
	}
	public void setPrice(int price){
		this.price = price;
	}
	
}
