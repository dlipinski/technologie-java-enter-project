package com.example.servletjspdemo.service;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import com.example.servletjspdemo.domain.Car;

public class BasketStorageService {

    private Map<Car, Integer> cars = new HashMap<Car, Integer>();

    public void add(Car car, int amount) {
        cars.put(new Car(car.getBrand(), car.getModel(), car.getPrice(), car.getId()), amount);
    }

    public Map<Car, Integer> getAllCars() {
        return cars;
    }

    public List<String> getAllIds() {
        List<String> a = new ArrayList<String>();

        for (Map.Entry<Car, Integer> entry : getAllCars().entrySet()) {
            a.add(entry.getKey().getId());
        }
        return a;
    }

    public void updateAmountById(String id, int amount) {
        for (Map.Entry<Car, Integer> entry : getAllCars().entrySet())
            if (entry.getKey().getId().contains(id)) {
                entry.setValue(entry.getValue() + amount);
            }
    }

    public void deleteCarById(String id) {

        for (Iterator<Map.Entry<Car, Integer>> it = cars.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<Car, Integer> entry = it.next();
            if (entry.getKey().getId().equals(id)) {
                it.remove();
            }
        }
    }
}
