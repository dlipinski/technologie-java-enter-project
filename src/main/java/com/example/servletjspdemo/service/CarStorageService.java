package com.example.servletjspdemo.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.example.servletjspdemo.domain.Car;

public class CarStorageService {

	private Map<Car, Integer> cars = new HashMap<Car, Integer>();
	private boolean isAdded = true;
	public void add(Car car,int amount){
		cars.put(new Car(car.getBrand(), car.getModel(),car.getPrice()),amount);
	}
	public void update(Car car,int amount){
		cars.put(car, amount);
	}
	public void delete(Car car){
		for(Iterator<Entry<Car, Integer>> it = cars.entrySet().iterator(); it.hasNext(); ) {
		      Entry<Car, Integer> entry = it.next();
		      if(entry.getKey().equals(car)) {
		        it.remove();
		      }
		}
	}
	public Map<Car, Integer> getAllCars(){
		return cars;
	}
	public void addExamples(){
		if(isAdded){
			isAdded=false;
			cars.put(new Car("Seat", "Mii",26000),1);
			cars.put(new Car("Opel", "Astra",36900),2);
			cars.put(new Car("Jeep", "Patriot",65900),3);
			cars.put(new Car("Ford", "Escape",32000),4);
			cars.put(new Car("Opel", "Combo",20000),4);
			cars.put(new Car("Fiat", "Ducato",18000),4);
			cars.put(new Car("Toyota", "Yaris",42000),4);
            cars.put(new Car("A", "1",18000),1);
            cars.put(new Car("B", "2",18000),2);
		}
	}
}