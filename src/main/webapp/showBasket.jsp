<%@page import="com.example.servletjspdemo.domain.Car"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>showAllCars</title>
</head>
<body>
<h1>YOUR BASKET<br>----------------------------------------------------------</h1>
<jsp:useBean id="storageB" class="com.example.servletjspdemo.service.BasketStorageService" scope="session" />
<form action="deleteCar.jsp" method="post">
<table>
	<tr>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
<%
	int allCash = 0;
	for (Map.Entry<Car, Integer> entry : storageB.getAllCars().entrySet()){
	if(entry.getValue()>0){
		allCash+=entry.getValue()*entry.getKey().getPrice();
%>
	<tr>
		<td><input name="toDelete" type="checkbox" value="<% out.print(entry.getKey().getId()); %>"><td>
		<td><% out.print(entry.getValue()); %>x</td>
    	<td><% out.print(entry.getKey().getBrand()); %></td>
    	<td><% out.print(entry.getKey().getModel()); %></td>
    	<td>(<% out.print(entry.getKey().getPrice()*entry.getValue()); %>$)</td>
  	</tr>
<% 
	}
	}
%>
</table>
<input type="submit" value="DELETE">
</form>
<br>
<u><b>Everything</b> : <% out.print(allCash); %>$</u><br><br>

<a href="#">END TRANS</a><br>
<a href="showAllCars.jsp">BUY ANOTHER CAR</a>

</body>
</html>