<%@page import="java.io.PrintWriter"%>
<%@page import="com.example.servletjspdemo.domain.Car"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>deleteCar</title>
</head>
<body>
<h1>DELETED CARS FROM BASKET<br>----------------------------------------------------------</h1>
<jsp:useBean id="storage" class="com.example.servletjspdemo.service.CarStorageService" scope="application" />
<jsp:useBean id="storageB" class="com.example.servletjspdemo.service.BasketStorageService" scope="session" />
<br>
<ul>
<%
	String[] ids = request.getParameterValues("toDelete");
	String brand = "";
	String model = "";
	int amount = 0;
	int price = 0;
	int allCash=0;
	if (ids.length>0)
		for (String id : ids){
				for (Map.Entry<Car, Integer> entry : storageB.getAllCars().entrySet()){
					if ((entry.getKey().getId().contains(id))){
						amount = entry.getValue();
						brand = entry.getKey().getBrand();
						model = entry.getKey().getModel();
						price = entry.getKey().getPrice();
						storageB.deleteCarById(id);
						out.print("<li>"+amount+" x "+brand+" "+model+"  - "+price+"$  ("+(amount * price)+"$)"+"</li>");
						allCash+=(amount * entry.getKey().getPrice());
					}

				}
			}
	
	
%>
</ul>
<b>Everything:</b> <% out.print(allCash); %>$
<p>
<a href="showBasket.jsp">SHOW BASKET</a>
</p>
<p>
<a href="showAllCars.jsp">BUY ANOTHER CAR</a>
</p>

</body>
</html>
