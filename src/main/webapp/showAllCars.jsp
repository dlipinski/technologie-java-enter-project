<%@page import="com.example.servletjspdemo.domain.Car"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>showAllCars</title>
</head>
<body>

<jsp:useBean id="storage" class="com.example.servletjspdemo.service.CarStorageService" scope="application" />
<h1>SHOP<br>----------------------------------------------------------</h1>
<form action="addCar.jsp" method="post">
<table>
	<tr>
		<th></th>
		<th>BRAND</th>
		<th>MODEL</th>
		<th>PRICE</th>
	</tr>
<%
	storage.addExamples();
	for (Map.Entry<Car, Integer> entry : storage.getAllCars().entrySet()){
	if(entry.getValue()>0){
%>
	<tr>
		
		<td>
		<select name="selected[]">
			<% for(int i=0; i <= entry.getValue();i++){ %>
				<option name="amount" value="<% out.print(i + ";"+entry.getKey().getId()); %>"> 
				<% out.print(i); %> 
				</option>
			<% } %>
		</select>
		</td>
    	<td><% out.print(entry.getKey().getBrand()); %></td>
    	<td><% out.print(entry.getKey().getModel()); %></td> 
    	<td><% out.print(entry.getKey().getPrice()); %>$</td>
  </tr>

<% 
	}
	}
%>
</table>
<input type="submit" value="ADD TO BASKET">
</form>
<a href="showBasket.jsp">SHOW BASKET</a>

</body>
</html>